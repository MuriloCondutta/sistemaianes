package br.com.patrimonio.ianes.config;
/**
 * 
 * Classe de configura��o do Hibernate.
 * 
 * Criado por Murilo Afonso Condutta em 26/03/2018.
 * 
 */

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class PersistenceConfig {

	/**
	 * 
	 * Cria um dataSource contnedo as informa��es de conex�o com o banco
	 * de dados que ser� utilizado no Hibernate.
	 * 
	 * @return - dataSource.
	 */
	@Bean
	public DataSource getDataSource() {
		
		BasicDataSource dataSource = new BasicDataSource();
		
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");

		dataSource.setUrl("jdbc:mysql://localhost:3306/patrimonio_mt3_25?serverTimezone=UTC");

		dataSource.setUsername("root");
		dataSource.setPassword("root132");

		return dataSource;
		
	}
	
	/**
	 * 
	 * Configura as propriedades do Hibernate.
	 * 
	 * @return - propriedades do Hibernate.
	 */
	public Properties getHibernateProperties() {
		
		Properties props = new Properties();
		
		props.setProperty("hibernate.show_sql", "true");
	
		props.setProperty("hibernate.hbm2ddl.auto", "update");
		
		props.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		
		return props;
		
	}
	
	/**
	 * 
	 * Cria um obejto que abrir� as conex�es com o banco de dados atrav�s do dataSource
	 * e do HibernateProperties que criamos nesta classe.
	 * 
	 * @return - objeto para conex�o.
	 */
	@Bean
	public LocalSessionFactoryBean getSessionFactory() {

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

		sessionFactory.setDataSource(getDataSource());

		sessionFactory.setHibernateProperties(getHibernateProperties());
		
		sessionFactory.setPackagesToScan("br.com.patrimonio.ianes.models");
		
		return sessionFactory;
		
	}
	
	/**
	 * 
	 * Pega o gerenciador da transaction
	 * 
	 * @return - gerenciador da transaction
	 */
	@Bean
	@Autowired
	public HibernateTransactionManager getTransactionManager() {
		
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(getSessionFactory().getObject());
		
		return transactionManager;
		
	}
	
}
