package br.com.patrimonio.ianes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FluxoPaginasController {

	@GetMapping(value = {"/", ""})
	public String abrePaginaLogin() {
		
		return "index";
		
	}
	
	@GetMapping(value = "/app")
	public String abrePaginaEntrada() {
		
		return "entrada";
		
	}
	
}
