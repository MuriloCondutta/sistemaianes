package br.com.patrimonio.ianes.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.patrimonio.ianes.dao.CategoriaDAO;
import br.com.patrimonio.ianes.models.Categoria;

@Controller
public class CategoriaController {
	
	@Autowired
	private CategoriaDAO categoriaDAO;
	
	@GetMapping("/app/adm/categoria/nova")
	public String abreCadastroCategoria(Model model) {
		
		model.addAttribute("categoria", new Categoria());
		return "Categoria/form";
		
	}
	
	@GetMapping("/app/adm/categoria/lista")
	public String abreListaCategoria(Model model) {
		
		model.addAttribute("categorias", categoriaDAO.buscarTodos());
		return "Categoria/lista";
		
	}
	
	@GetMapping("/app/adm/categoria/deletar")
	public String deletarCategoria(@RequestParam(required = true) Long id) {
		
		Categoria categoriaBuscada = categoriaDAO.buscar(id);
		categoriaDAO.deletar(categoriaBuscada);
		return "redirect:/app/adm/categoria/lista";
		
	}
	
	@GetMapping("/app/adm/categoria/alterar")
	public String abreAlterarCategoria(@RequestParam(required = true) Long id, Model model) {
		
		model.addAttribute("categoria", categoriaDAO.buscar(id));
		return "Categoria/form";
		
	}
	
	@PostMapping("/app/adm/categoria/salvar")
	public String salvarCategoria(@Valid Categoria categoria, BindingResult brCategoria) {
		
		if(brCategoria.hasErrors()) {
			
			return "Categoria/form";
			
		}
		
		if(categoria.getId() == null) {
			
			categoriaDAO.persistir(categoria);
			
		} else {
			
			Categoria categoriaBuscada = categoriaDAO.buscar(categoria.getId());
			categoriaBuscada.setNome(categoria.getNome());
			
			// Verificando
			
			System.out.println(categoriaBuscada.getId());
			
			categoriaDAO.alterar(categoriaBuscada);
			
			
			
		}
		
		return "redirect:/app";
		
	}

}
