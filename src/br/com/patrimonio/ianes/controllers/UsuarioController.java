package br.com.patrimonio.ianes.controllers;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.patrimonio.ianes.dao.UsuarioDAO;
import br.com.patrimonio.ianes.models.TipoUsuario;
import br.com.patrimonio.ianes.models.Usuario;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@RequestMapping("/app/adm/usuario/novo")
	public String abrePaginaNovo(Model model) {
		
		model.addAttribute("usuario", new Usuario());
		return "Usuario/form";
		
	}
	
	@PostMapping("/app/adm/usuario/salvar")
	public String salvar(@Valid Usuario usuario, BindingResult brUsuario,
			@RequestParam(name = "confirmacaoSenha", required = false) String confirmarSenha,
			@RequestParam(name = "isAdministrador", required = false) Boolean isAdministrador) {
		
		if(usuario.getId() == null) {
			
			// Cheacndo os campos de senha
			if(!confirmarSenha.equals(usuario.getSenha())) {
				
				brUsuario.addError(new FieldError("usuario", "senha", "As senhas n�o coincidem"));
				
			}
			
			// Checando o email
			if(usuarioDao.buscarPorEmail(usuario.getEmail()) != null) {
				
				brUsuario.addError(new FieldError("usuario", "email", "O e-mail selecionado j� existe"));
				
			}
			
			// Checando se h� erros
			if(brUsuario.hasErrors()) {
				
				return "Usuario/form";
				
			}
			
		} else {
			
			if(brUsuario.hasFieldErrors("nome") || brUsuario.hasFieldErrors("sobrenome")) {
				
				return "Usuario/form";
				
			}
			
		} // Fim do cadastro
		
		// Checkbox do isAdm
		if(isAdministrador != null) {
			
			usuario.setTipo(TipoUsuario.ADMINISTRADOR);
			
		} else {
			
			usuario.setTipo(TipoUsuario.COMUM);
			
		}
		
		if(usuario.getId() == null) {
			
			usuario.hashearSenha();
			usuarioDao.persistir(usuario);
			
		} else {
			
			Usuario usuarioBuscado = usuarioDao.buscar(usuario.getId());
			usuarioBuscado.setNome(usuario.getNome());
			usuarioBuscado.setSobrenome(usuario.getSobrenome());
			usuarioBuscado.setTipo(usuario.getTipo());
			
			usuarioDao.alterar(usuarioBuscado);
			
		}
		
		return "redirect:/app";
		
	}
	
	@GetMapping("/app/adm/usuario/deletar")
	public String deletar(@RequestParam(required=  true) Long id) {
		
		Usuario usuarioBuscado = usuarioDao.buscar(id);
		usuarioDao.deletar(usuarioBuscado);
		
		return "redirect:/app/adm/usuario/lista";
		
	}
	
	@PostMapping(value = {"/usuario/autenticar"})
	public String autenticar(@Valid Usuario usuario, BindingResult brUsuario, HttpSession session) {
		
		usuario.hashearSenha();
		
		Usuario usuarioBuscado = usuarioDao.buscarPorEmail(usuario.getEmail());
		
		if(usuarioBuscado == null) {
			
			brUsuario.addError(new FieldError("usuario", "email", "Email ou senha incorretos"));
			
		}
		
		if(brUsuario.hasFieldErrors("email")) {
			
			return "index";
			
		}
		
		session.setAttribute("usuarioAutenticado", usuarioBuscado);
		
		return "redirect:/app/";
		
	}
	
	@GetMapping("app/adm/usuario/lista")
	public String listarUsuarios(Model model) {
		
		model.addAttribute("usuarios", usuarioDao.buscarTodos());
		return "Usuario/lista";
		
	}
	
	@GetMapping("app/adm/usuario/alterar")
	public String abreAlterar(@RequestParam(required = true) Long id, Model model) {
		
		model.addAttribute("usuario", usuarioDao.buscar(id));
		return "Usuario/form";
		
	}
	
	@GetMapping("/sair")
	public String sair(HttpSession session) {
		
		session.removeAttribute("usuarioAutenticado");
		
		return "redirect:/";
		
	}
	
}
