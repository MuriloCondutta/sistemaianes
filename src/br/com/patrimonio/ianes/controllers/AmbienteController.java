package br.com.patrimonio.ianes.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.patrimonio.ianes.dao.AmbienteDAO;
import br.com.patrimonio.ianes.models.Ambiente;

@Controller
public class AmbienteController {
	
	@Autowired
	private AmbienteDAO ambienteDAO;
	
	@GetMapping("/app/adm/ambiente/novo")
	public String abreCadastroAmbiente(Model model) {
		
		model.addAttribute("ambiente", new Ambiente());
		return "Ambiente/form";
		
	}
	
	@GetMapping("/app/adm/ambiente/lista")
	public String abreListaAmbiente(Model model) {
		
		model.addAttribute("ambientes", ambienteDAO.buscarTodos());
		return "Ambiente/lista";
		
	}
	
	@GetMapping("/app/adm/ambiente/deletar")
	public String deletarAmbiente(@RequestParam(required = true) Long id) {
		
		Ambiente ambienteBuscado = ambienteDAO.buscar(id);
		ambienteDAO.deletar(ambienteBuscado);
		return "redirect:/app/adm/ambiente/lista";
		
	}
	
	@GetMapping("/app/adm/ambiente/alterar")
	public String abreAlterarAmbiente(@RequestParam(required = true) Long id, Model model) {
		
		model.addAttribute("ambiente", ambienteDAO.buscar(id));
		return "Ambiente/form";
		
	}
	
	@PostMapping("/app/adm/ambiente/salvar")
	public String salvarAmbiente(@Valid Ambiente ambiente, BindingResult brAmbiente) {
		
		if(brAmbiente.hasErrors()) {
			
			return "Ambiente/form";
			
		}
		
		if(ambiente.getId() == null) {
			
			ambienteDAO.persistir(ambiente);
			
		} else {
			
			Ambiente ambienteBuscado = ambienteDAO.buscar(ambiente.getId());
			ambienteBuscado.setNome(ambiente.getNome());
			
			// Verificando
			
			System.out.println(ambienteBuscado.getId());
			
			ambienteDAO.alterar(ambienteBuscado);
			
			
			
		}
		
		return "redirect:/app";
		
	}

}
