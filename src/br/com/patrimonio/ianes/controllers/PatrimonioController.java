package br.com.patrimonio.ianes.controllers;

import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.patrimonio.ianes.dao.CategoriaDAO;
import br.com.patrimonio.ianes.dao.PatrimonioDAO;
import br.com.patrimonio.ianes.models.Patrimonio;
import br.com.patrimonio.ianes.models.Usuario;

@Controller
public class PatrimonioController {
	
	@Autowired
	private PatrimonioDAO patrimonioDAO;
	
	@Autowired
	private CategoriaDAO categoriaDAO;
	
	@RequestMapping("/app/adm/patrimonio/novo")
	public String abrePaginaNovo(Model model) {
		
		model.addAttribute("patrimonio", new Patrimonio());
		model.addAttribute("categorias", categoriaDAO.buscarTodos());
		return "Patrimonio/form";
		
	}
	
	@PostMapping("/app/adm/patrimonio/salvar")
	public String salvar(@Valid Patrimonio patrimonio, BindingResult brPatrimonio, HttpSession session, Model model) {
		
		if(brPatrimonio.hasErrors()) {
			
			model.addAttribute("categorias", categoriaDAO.buscarTodos());
			return "Patrimonio/form";
			
		}
		
		if(patrimonio.getId() == null) {
			
			patrimonio.setDataCadastro(new Date());
			
			Usuario usuarioNaSessao = (Usuario) session.getAttribute("usuarioAutenticado");
			
			patrimonio.setUsuarioCriador(usuarioNaSessao);
			patrimonioDAO.persistir(patrimonio);
			
		} else {
			
			Patrimonio patrimonioBuscado = patrimonioDAO.buscar(patrimonio.getId());
			patrimonioBuscado.setNome(patrimonio.getNome());
			patrimonioBuscado.setCategoria(patrimonio.getCategoria());
			patrimonioDAO.alterar(patrimonioBuscado);
			
		}
		
		return "redirect:/app";
		
	}
	
	@GetMapping("/app/adm/patrimonio/deletar")
	public String deletar(@RequestParam(required=  true) Long id) {
		
		Patrimonio patrimonioBuscado = patrimonioDAO.buscar(id);
		patrimonioDAO.deletar(patrimonioBuscado);
		
		return "redirect:/app/patrimonio/lista";
		
	}
	
	@GetMapping("app/patrimonio/lista")
	public String listarUsuarios(Model model) {
		
		model.addAttribute("patrimonios", patrimonioDAO.buscarTodos());
		return "Patrimonio/lista";
		
	}
	
	@GetMapping("app/adm/patrimonio/alterar")
	public String abreAlterar(@RequestParam(required = true) Long id, Model model) {
		
		model.addAttribute("patrimonio", patrimonioDAO.buscar(id));
		model.addAttribute("categorias", categoriaDAO.buscarTodos());
		return "Patrimonio/form";
		
	}

}
