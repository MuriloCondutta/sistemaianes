package br.com.patrimonio.ianes.interceptors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import br.com.patrimonio.ianes.models.TipoUsuario;
import br.com.patrimonio.ianes.models.Usuario;

@Component
public class AutenticacaoInterceptor extends HandlerInterceptorAdapter{

	@Autowired
	private HttpSession session;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		// Para entrar no app
		
		Boolean necessitaAutenticacao = request.getRequestURI().contains("/app");
		
		Usuario usuarioAutenticado = (Usuario) session.getAttribute("usuarioAutenticado");
		
		Boolean usuarioEstaAutenticado = usuarioAutenticado!= null;
		
		// Para administradores
		
		Boolean necessitaSerAdm = request.getRequestURI().contains("/adm");
		
		// An�lises de casos
		
		if(necessitaAutenticacao) {
			
			if (usuarioEstaAutenticado) {
				
				if (necessitaSerAdm && usuarioAutenticado.getTipo() != TipoUsuario.ADMINISTRADOR) {
					
					response.sendError(403);
					
					return false;
					
				}
				
			} else {
				
				response.sendError(401);
				
				return false;
				
			} // Fim do se o usu�rio est� autenticado
			
		}
		
		return true;
		
	} // Fim do preHandle
	
}
