package br.com.patrimonio.ianes.dao;

import java.util.List;

import br.com.patrimonio.ianes.models.ItensPatrimonio;

public interface ItensPatrimonioDAO extends DAO<ItensPatrimonio>{

	public List<ItensPatrimonio> buscarPorPatrimonioId(Long patrimonioId);
	
}
