package br.com.patrimonio.ianes.dao;

import br.com.patrimonio.ianes.models.Usuario;

public interface UsuarioDAO extends DAO<Usuario>{
	
	public Usuario buscarPorEmail(String email);
	
}
