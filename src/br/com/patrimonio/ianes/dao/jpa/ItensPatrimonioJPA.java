package br.com.patrimonio.ianes.dao.jpa;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.patrimonio.ianes.dao.ItensPatrimonioDAO;
import br.com.patrimonio.ianes.models.ItensPatrimonio;

@Repository
@Transactional
public class ItensPatrimonioJPA implements ItensPatrimonioDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public void persistir(ItensPatrimonio obj) {
		
		sessionFactory.getCurrentSession().persist(obj);
		
	}

	@Override
	public void alterar(ItensPatrimonio obj) {
		
		sessionFactory.getCurrentSession().update(obj);
		
	}

	@Override
	public void deletar(ItensPatrimonio obj) {
		
		sessionFactory.getCurrentSession().delete(obj);		
		
	}

	@Override
	public ItensPatrimonio buscar(Long id) {
		
		String hql = "FROM ItensPatrimonio i WHERE i.id = :id";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		query.setParameter("id", id);
		
		List<ItensPatrimonio> itens = query.list();
		
		if(!itens.isEmpty()) {
			
			return itens.get(0);
			
		} else {
			
			return null;
			
		}
		
	}

	@Override
	public List<ItensPatrimonio> buscarTodos() {
		
		String hql = "FROM ItensPatrimonio";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		return query.list();
		
	}

	@Override
	public List<ItensPatrimonio> buscarPorPatrimonioId(Long patrimonioId) {
		
		String hql = "FROM ItensPatrimonio i WHERE i.patrimonioId = :idDoPatrimonio";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		
		query.setParameter("idDoPatrimonio", patrimonioId);
		
		return query.list();
		
	}

}
