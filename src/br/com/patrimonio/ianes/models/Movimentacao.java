package br.com.patrimonio.ianes.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sun.istack.internal.NotNull;

@Entity
public class Movimentacao {

	// Atributos
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = false)
	@NotNull
	private Date dataMovimentacao;
	
	@ManyToOne // [n] movimentacao <-> [1] usuario
	@JoinColumn(nullable = false)
	@NotNull
	private Usuario usuarioQueMovimentou;
	
	@ManyToOne // [n] movimentacao <-> [1] ambiente
	@JoinColumn(nullable = false)
	@NotNull
	private Ambiente ambienteO;
	
	@ManyToOne // [n] movimentacao <-> [1] ambiente
	@JoinColumn(nullable = false)
	@NotNull
	private Ambiente ambienteD;
	
	// Getters and Setters

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDataMovimentacao() {
		return dataMovimentacao;
	}

	public void setDataMovimentacao(Date dataMovimentacao) {
		this.dataMovimentacao = dataMovimentacao;
	}

	public Usuario getUsuarioQueMovimentou() {
		return usuarioQueMovimentou;
	}

	public void setUsuarioQueMovimentou(Usuario usuarioQueMovimentou) {
		this.usuarioQueMovimentou = usuarioQueMovimentou;
	}

	public Ambiente getAmbienteO() {
		return ambienteO;
	}

	public void setAmbienteO(Ambiente ambienteO) {
		this.ambienteO = ambienteO;
	}

	public Ambiente getAmbienteD() {
		return ambienteD;
	}

	public void setAmbienteD(Ambiente ambienteD) {
		this.ambienteD = ambienteD;
	}
	
	
	
}
