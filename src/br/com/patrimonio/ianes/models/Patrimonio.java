package br.com.patrimonio.ianes.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;

import com.sun.istack.internal.NotNull;

@Entity
public class Patrimonio {

	// Atributos
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(length = 40, nullable = false, unique = true)
	@NotNull
	@Size(min = 1, max = 40)
	private String nome;
	
	@Column(nullable = false, unique = false)
	@NotNull
	private Date dataCadastro;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "categoria")
	@NotNull
	private Categoria categoria;
	
	@ManyToOne // [n] patrimonio <-> [1] usuario
	@JoinColumn(nullable = false, name = "usuario_criador")
	@NotNull
	private Usuario usuarioCriador;

	// Getters and Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}	
	
}
