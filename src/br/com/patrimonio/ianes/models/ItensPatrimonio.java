package br.com.patrimonio.ianes.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.sun.istack.internal.NotNull;

@Entity
public class ItensPatrimonio {
	
	// Atributos

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne // [n] item <-> [1] patrimonio
	@JoinColumn(nullable = false, name = "patrimonio")
	@NotNull
	private Patrimonio patrimonioId;
	
	@ManyToOne // [n] item <-> [1] ambiente
	@JoinColumn(nullable = false, name = "ambiente_atual")
	@NotNull
	private Ambiente ambienteAtual;
	
	@ManyToOne // [n] item <-> [1] usuario
	@JoinColumn(nullable = false, name = "usuario_criador")
	@NotNull
	private Usuario usuarioCriador;
	
	@ManyToOne // [n] item <-> [1] movimentacao
	@JoinColumn(nullable = true, name = "ultima_movimentacao")
	private Movimentacao ultimaMovimentacao;
	
	// Getters and Setters

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Patrimonio getPatrimonioId() {
		return patrimonioId;
	}

	public void setPatrimonioId(Patrimonio patrimonioId) {
		this.patrimonioId = patrimonioId;
	}

	public Ambiente getAmbienteAtual() {
		return ambienteAtual;
	}

	public void setAmbienteAtual(Ambiente ambienteAtual) {
		this.ambienteAtual = ambienteAtual;
	}

	public Usuario getUsuarioCriador() {
		return usuarioCriador;
	}

	public void setUsuarioCriador(Usuario usuarioCriador) {
		this.usuarioCriador = usuarioCriador;
	}

	public Movimentacao getUltimaMovimentacao() {
		return ultimaMovimentacao;
	}

	public void setUltimaMovimentacao(Movimentacao ultimaMovimentacao) {
		this.ultimaMovimentacao = ultimaMovimentacao;
	}
	
	
	
}
