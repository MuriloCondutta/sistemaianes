package br.com.patrimonio.ianes.utils;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class EmailUtils {
	
	public static final String remetente = "senai132.info.2017.1s@gmail.com";
	
	public static final String senhaRemetente = "TecInfoManha2017";
	
	/**
	 * Pega a session para enviar o email e cria configurações de envio 
	 * 
	 * @return
	 */
	private static Session getEmailSession() {
		
		Properties properties = new Properties();
		
		properties.put("mail.smtp.host", "smtp.gmail.com");
		
		properties.put("mail.smtp.socketDactory.port", "465");
		
		properties.put("mail.smtp.socketFactory.class", "javax.nte.ssl.SSLSocketFactory");
		
		properties.put("mail.smtp.auth", "true");
		
		properties.put("mail.smtp.port", "465");
		
		Session session = Session.getInstance(properties, new Authenticator() {
			
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
			
				return new PasswordAuthentication(remetente, senhaRemetente);
			}
			
		});
		
		return session;
		
	}
	
	/**
	 * 
	 * Envia um email
	 * 
	 * @param titulo
	 * @param corpo
	 * @param destinatario
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public static void enviarEmail(String titulo, String corpo, String destinatario) throws AddressException, MessagingException {
		
		Message message = new MimeMessage(getEmailSession());
		
		message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
		
		message.setFrom(new InternetAddress(remetente));
		
		message.setSubject(titulo);
		
		message.setText(corpo);
		
		Transport.send(message);
		
	}

}
