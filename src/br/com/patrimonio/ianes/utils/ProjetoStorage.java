package br.com.patrimonio.ianes.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Grava arquivos dentro do pr�rpio projeto
 * 
 * @author 44891069805
 *
 */
@Component
public class ProjetoStorage {
	
	/**
	 * Guarda informa��es sobre onde est� rodando o projeto
	 */
	@Autowired
	private ServletContext context;
	
	public String pegaCaminhoDoProjeto() {
		
		return context.getRealPath("");
		
	}
	
	public void armazenarFotoPerfil(String nomeDoArquivo, byte[] dadosDoArquivo) throws IOException {
		
		String caminhoDasFotos = pegaCaminhoDoProjeto() + "/Assets/Fotos";
		String caminhoDoArquivo = caminhoDasFotos + "/" + nomeDoArquivo;
		
		// Cria pasta de fotos, caso ela n�o exista
		File pastaDaFoto = new File(caminhoDasFotos);
		
		if(!pastaDaFoto.exists()) {
			
			pastaDaFoto.mkdirs();
			
		}
		
		// Cria o arquivo na pasta
		File arquivoDaFoto = new File(caminhoDoArquivo);
		
		// Caso ele exista, exclua-o
		if(arquivoDaFoto.exists()) {
			
			arquivoDaFoto.delete();
			
		}
		
		arquivoDaFoto.createNewFile();
		
		// Salva o conte�do no arquivo
		BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(arquivoDaFoto));
		bos.write(dadosDoArquivo);
		bos.close();
		
	}

}
