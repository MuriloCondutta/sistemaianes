package br.com.patrimonio.ianes.jobs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import br.com.patrimonio.ianes.dao.UsuarioDAO;
import br.com.patrimonio.ianes.models.TipoUsuario;
import br.com.patrimonio.ianes.models.Usuario;

@Component
public class CriaAdminJob implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent context) {
		
		Usuario usuarioAdmin = new Usuario();
		
		usuarioAdmin.setEmail("admin@admin.com");
		usuarioAdmin.setNome("Administrador");
		usuarioAdmin.setSenha("admin");
		usuarioAdmin.setSobrenome("do sistema");
		usuarioAdmin.setTipo(TipoUsuario.ADMINISTRADOR);
		
		if(usuarioDAO.buscarPorEmail(usuarioAdmin.getEmail()) == null) {
			
			usuarioDAO.persistir(usuarioAdmin);
			
		} else {
			
			System.out.println("Usu�rio administrador j� existente");
			
		}
		
	}	
	
}
