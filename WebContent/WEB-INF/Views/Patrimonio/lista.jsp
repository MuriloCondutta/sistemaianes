<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/app/adm/patrimonio/alterar" var="alterarPatrimonio"></c:url>
<c:url value="/app/adm/patrimonio/deletar" var="deletarPatrimonio"></c:url>
<c:url value="/app/adm/itens/lista" var="listaItens"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>

</head>
<body>

	<h1>Patrimônios</h1>	

	<div>
	
		<c:forEach items="${ patrimonios }" var="patriomonio">
		
		<div onclick="${ listaItens }?patrimonioId=${patrimonio.id}">
		
			<h2>Nome:</h2>
			<p>${ patriomonio.nome }</p>
			
			<h2>Usuario quecadastrou:</h2>
			<p>${ patriomonio.usuarioCriador.nome }</p>
			
			<h2>Data de cadastro:</h2>
			<p>${ patriomonio.dataCadastro }</p>
			
			<h2>Categoria:</h2>
			<p>${ patriomonio.categoria.nome }</p>
			
			<a href="${ alterarPatrimonio }?id=${ patriomonio.id }">AlTERAR</a>
			<a href="${ deletarPatrimonio }?id=${ patriomonio.id }">DELETAR</a>	
			
			<hr/>	
			
		</div>
		
		</c:forEach>
	
	</div>

</body>
</html>