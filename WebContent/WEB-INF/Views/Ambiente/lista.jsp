<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/app/adm/ambiente/alterar" var="alterarAmbiente"></c:url>
<c:url value="/app/adm/ambiente/deletar" var="deletarAmbiente"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>

</head>
<body>

	<h1>Ambientes</h1>	

	<div>
	
		<c:forEach items="${ ambientes }" var="ambiente">
		
			<h2>Nome:</h2>
			<p>${ ambiente.nome }</p>
			
			<a href="${ alterarAmbiente }?id=${ ambiente.id }">AlTERAR</a>
			<a href="${ deletarAmbiente }?id=${ ambiente.id }">DELETAR</a>	
			
			<hr/>	
		
		</c:forEach>
	
	</div>

</body>
</html>