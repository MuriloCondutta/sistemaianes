<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:url value="/app/adm/categoria/salvar" var="SalvarCategoria"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>
	
	<style type="text/css">
	
		form{
		
		display: flex;
		flex-wrap: wrap;
		width: 25%;
		
		}
	
	</style>

</head>
<body>

	<form:form modelAttribute="categoria" method="post" action="${ SalvarCategoria }">

		<form:hidden path="id"/>

		Nome:		
		<form:input path="nome"/>
		<form:errors path="nome" element="div"/>
		
		<button type="submit" >Salvar</button>
	
	</form:form>

</body>
</html>