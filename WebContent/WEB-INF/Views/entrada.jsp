<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/sair" var="sair"></c:url>
<c:url value="/app/adm/usuario/novo" var="cadastraUsuario"></c:url>
<c:url value="/app/adm/usuario/lista" var="listarUsuario"></c:url>
<c:url value="/app/adm/patrimonio/novo" var="cadastraPatrimonio"/>
<c:url value="/app/patrimonio/lista" var="listaPatrimonio"/>
<c:url value="/app/adm/ambiente/novo" var="cadastraAmbiente"/>
<c:url value="/app/adm/ambiente/lista" var="listaAmbiente"/>
<c:url value="/app/adm/categoria/nova" var="cadastraCategoria"/>
<c:url value="/app/adm/categoria/lista" var="listaCategoria"/>

<!DOCTYPE html>
<html>
<head>

	<c:import url="Templates/head.jsp"/>

</head>
<body>

	<h1>Bem-Vindooooooooo</h1>
	
	<a href="${ cadastraUsuario }">Adicionar usuário</a>
	<a href="${ listarUsuario }">Listar usuarios</a>
	<a href="${ cadastraAmbiente }">Cadastrar Ambientes</a>
	<a href="${ listaAmbiente }">Listar ambientes</a>
	<a href="${ cadastraCategoria }">Cadastrar Categorias</a>
	<a href="${ listaCategoria }">Listar categorias</a>
	<a href="${ cadastraPatrimonio }">Cadastrar patrimônios</a>
	<a href="${ listaPatrimonio }">Listar patrimônios</a>
	<a href="${ sair }">Sair</a>

</body>
</html>