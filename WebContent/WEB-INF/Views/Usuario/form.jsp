<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:url value="/app/adm/usuario/salvar" var="SalvarUsuario"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>
	
	<style type="text/css">
	
		form{
		
		display: flex;
		flex-wrap: wrap;
		width: 25%;
		
		}
	
	</style>

</head>
<body>

	<form:form modelAttribute="usuario" method="post" action="${ SalvarUsuario }">

		<form:hidden path="id"/>

		Nome:		
		<form:input path="nome"/>
		<form:errors path="nome" element="div"/>
		
		Sobrenome:
		<form:input path="sobrenome"/>
		<form:errors path="sobrenome" element="div"/>
		
		Email:
		<form:input path="email"/>
		<form:errors path="email" element="div"/>
		
		<c:if test="${ empty usuario.id }">
		
			Senha:
			<form:password path="senha"/>
			<form:errors path="senha" element="div"/>
			
			ConfirmarSenha:
			<input type="password" name="confirmacaoSenha"/>
			<form:errors path="senha" element="div"/>
			
		</c:if>
		
		Adm:
		<input type="checkbox" name="isAdministrador" ${ usuario.tipo eq 'ADMINISTRADOR' ? 'checked' : ''}/>
		
		<button type="submit" >Salvar</button>
	
	</form:form>

</body>
</html>