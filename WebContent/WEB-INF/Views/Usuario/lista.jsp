<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/app/adm/usuario/alterar" var="alterarUsuario"></c:url>
<c:url value="/app/adm/usuario/deletar" var="deletarUsuario"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>

</head>
<body>

	<h1>Usuários</h1>	

	<div>
	
		<c:forEach items="${ usuarios }" var="usuario">
		
			<h2>Nome:</h2>
			<p>${ usuario.nome }</p>
			
			<h2>Sobrenome:</h2>
			<p>${ usuario.sobrenome }</p>
			
			<h2>Email:</h2>
			<p>${ usuario.email }</p>
			
			<h2>Tipo:</h2>
			<p>${ usuario.tipo }</p>
			
			<a href="${ alterarUsuario }?id=${ usuario.id }">AlTERAR</a>
			<a href="${ deletarUsuario }?id=${ usuario.id }">DELETAR</a>	
			
			<hr/>	
		
		</c:forEach>
	
	</div>

</body>
</html>