<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:url value="/app/adm/categoria/alterar" var="alterarCategoria"></c:url>
<c:url value="/app/adm/categoria/deletar" var="deletarCategoria"></c:url>

<!DOCTYPE html>
<html>
<head>

	<c:import url="../Templates/head.jsp"></c:import>

</head>
<body>

	<h1>Categorias</h1>	

	<div>
	
		<c:forEach items="${ categorias }" var="categoria">
		
			<h2>Nome:</h2>
			<p>${ categoria.nome }</p>
			
			<a href="${ alterarCategoria }?id=${ categoria.id }">AlTERAR</a>
			<a href="${ deletarCategoria }?id=${ categoria.id }">DELETAR</a>	
			
			<hr/>	
		
		</c:forEach>
	
	</div>

</body>
</html>